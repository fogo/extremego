package feed

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	// dependencies
}

func NewHandler() Handler {
	return Handler{}
}

func (h Handler) Router(router gin.IRouter) {
	router.GET("/feed", h.feed)
}

func (h Handler) feed(c *gin.Context) {
	food := c.Query("food")
	if food == "" {
		c.AbortWithError(http.StatusBadRequest, errors.New("you need to feed your dragon, if not it will die!!! "))
		return
	}

	msg := fmt.Sprintf("food:%s", food)
	c.String(http.StatusOK, msg)
}