package errorreporting

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func NewMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		errors := c.Errors.ByType(gin.ErrorTypeAny)

		if len(errors) > 0 {
			currentStatus := c.Writer.Status()
			// When using default Errors from gin the status is 200
			if currentStatus == http.StatusOK {
				currentStatus = http.StatusInternalServerError
			}

			e := errors[0].Err
			c.String(currentStatus, e.Error())
		}
	}
}