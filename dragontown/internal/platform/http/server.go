package http

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
)

// Server representation of our http Server
type Server struct {
	engine          *gin.Engine
	httpAddr        string
	shutdownTimeout uint
}

// Handler defines the handler behaviour
type Handler interface {
	Router(router gin.IRouter)
}

// NewServer returns a Server instance with the engine and his httpAddr
func NewServer(ctx context.Context, host string, port, shutdownTimeout uint) (context.Context, Server) {
	httpAdrr := fmt.Sprintf("%s:%d", host, port)
	return serverContext(ctx), Server{
		engine:          gin.Default(),
		httpAddr:        httpAdrr,
		shutdownTimeout: shutdownTimeout,
	}
}

// AddHandler add handlers into the engine router
func (s *Server) AddHandler(handler Handler) {
	handler.Router(s.engine)
}

// AddMiddlewares adding middlewares to use during the execution of our server
func (s *Server) AddMiddlewares(middlewares ...gin.HandlerFunc) {
	s.engine.Use(middlewares...)
}

// Run execute the server and running until the context be cancelled
func (s Server) Run(ctx context.Context) error {
	srv := &http.Server{
		Addr:    s.httpAddr,
		Handler: s.engine,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("server shut down", err)
		}
	}()

	<-ctx.Done()
	ctxShutDown, cancel := context.WithTimeout(context.Background(), time.Duration(s.shutdownTimeout)*time.Second)
	defer func() {
		cancel()
	}()

	return srv.Shutdown(ctxShutDown)
}

func serverContext(ctx context.Context) context.Context {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	ctx, cancel := context.WithCancel(ctx)
	go func() {
		<-c
		cancel()
	}()

	return ctx
}
