package feed

import (
	"fmt"

	"github.com/spf13/cobra"
)

func NewFeedCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "feed",
		Short: "feed your dragon with any type of food",
		RunE: func(cmd *cobra.Command, args []string) error {
			food, err := cmd.Flags().GetString("food")
			if err != nil {
				return err
			}

			if food == "" {
				return cmd.Help()
			}

			fmt.Println("food:", food)
			return nil
		},
	}

	cmd.Flags().StringP("food", "f", "", "any kind of food")
	return cmd
}
