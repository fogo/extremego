package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/fogo/extremego/dragontown/internal/platform/cli/cmd/feed"
)

func NewDragonTownCmd() *cobra.Command {
	rootCmd := &cobra.Command{
		Use:   "dragontown",
		Short: "A simple demo of cli",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			fmt.Println("Welcome to Dragon Town, young adventurer")
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			info, err := cmd.Flags().GetBool("info")
			if err != nil {
				return err
			}

			if info {
				fmt.Println("Red Dragon, exp: 9999, lvl: 25")
			} else {
				cmd.Help()
			}
			return nil
		},
	}

	rootCmd.ResetFlags()
	rootCmd.Flags().BoolP("info", "i", false, "show dragon info")

	rootCmd.AddCommand(feed.NewFeedCmd())
	return rootCmd

}
