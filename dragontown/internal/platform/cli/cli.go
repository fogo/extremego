package cli

import "gitlab.com/fogo/extremego/dragontown/internal/platform/cli/cmd"

func Run() error {
	rootCmd := cmd.NewDragonTownCmd()
	return rootCmd.Execute()
}
