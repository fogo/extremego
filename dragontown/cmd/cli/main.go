package main

import (
	"log"

	"gitlab.com/fogo/extremego/dragontown/internal/platform/cli"
)

func main() {
	if err := cli.Run(); err != nil {
		log.Fatal(err)
	}
}
