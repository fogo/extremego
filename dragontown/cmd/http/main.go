package main

import (
	"context"
	"log"

	"gitlab.com/fogo/extremego/dragontown/internal/platform/http"
	"gitlab.com/fogo/extremego/dragontown/internal/platform/http/handler/feed"
	"gitlab.com/fogo/extremego/dragontown/internal/platform/http/handler/training"
	"gitlab.com/fogo/extremego/dragontown/internal/platform/http/middleware/errorreporting"
)

const (
	host = "localhost"
	port uint = 4000
	shutdownTime = 15
)

func main() {
	feedHandler := feed.NewHandler()
	trainingHandler := training.NewHandler()

	ctx, srv := http.NewServer(context.Background(), host, port, shutdownTime)
	srv.AddMiddlewares(errorreporting.NewMiddleware())

	srv.AddHandler(feedHandler)
	srv.AddHandler(trainingHandler)


	if err := srv.Run(ctx); err != nil {
		log.Fatal(err)
	}
}


