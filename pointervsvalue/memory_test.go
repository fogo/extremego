package pointervsvalue

import (
	"fmt"
	"os"
	"runtime/trace"
	"testing"
)

func BenchmarkByCopy(b *testing.B) {
	var dragon Dragon

	f, err := os.Create("copy.out")
	if err != nil {
		b.Fatal(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		b.Fatal(err)
	}

	for i := 0; i < b.N; i++ {
		dragon = byCopy()
	}

	trace.Stop()
	b.StopTimer()

	_ = fmt.Sprintf("%v", dragon.name)
}

func BenchmarkByPointer(b *testing.B) {
	var dragon *Dragon

	f, err := os.Create("pointer.out")
	if err != nil {
		b.Fatal(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		b.Fatal(err)
	}

	for i := 0; i < b.N; i++ {
		dragon = byPointer()
	}

	trace.Stop()
	b.StopTimer()

	_ = fmt.Sprintf("%v", dragon.name)
}