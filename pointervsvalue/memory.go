package pointervsvalue

type Dragon struct {
	name       string
	age        int
	experience float64
	isActive   bool
}

func byCopy() Dragon {
	return Dragon{
		name:       "Eustaquio",
		age:        50,
		experience: 999.5,
		isActive:   true,
	}
}

func byPointer() *Dragon {
	return &Dragon{
		name:       "Eustaquio",
		age:        50,
		experience: 999.5,
		isActive:   true,
	}
}